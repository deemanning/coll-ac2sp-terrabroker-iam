XARGS_CMD ?= xargs -I {}
BIN_DIR ?= ${HOME}/bin
PATH := $(BIN_DIR):${PATH}

MAKEFLAGS += --no-print-directory
SHELL := bash
.SHELLFLAGS := -eu -o pipefail -c
.SUFFIXES:

.PHONY: %/install %/lint release/%
.PHONY: validate/% plan/% apply/% destroy/%

guard/env/%:
	@ _=$(or $($*),$(error Make/environment variable '$*' not present))

guard/program/%:
	@ which $* > /dev/null || $(MAKE) $*/install

$(BIN_DIR):
	@ echo "[make]: Creating directory '$@'..."
	mkdir -p $@

zip/install:
	@ echo "[$@]: Installing $(@D)..."
	apt-get install zip -y
	@ echo "[$@]: Completed successfully!"

jq/install: JQ_URL ?= https://github.com/stedolan/jq/releases/download/jq-1.5/jq-linux64
jq/install: | $(BIN_DIR)
	@ echo "[$@]: Installing $(@D)..."
	@ echo "[$@]: JQ_URL=$(JQ_URL)"
	curl -sSL "$(JQ_URL)" -o jq
	chmod +x ./jq
	mv ./jq "$(BIN_DIR)"
	jq --version
	@ echo "[$@]: Completed successfully!"

terraform/install: TERRAFORM_VERSION ?= $(shell curl -sSL https://checkpoint-api.hashicorp.com/v1/check/terraform | jq -r -M '.current_version')
terraform/install: TERRAFORM_URL ?= https://releases.hashicorp.com/terraform/$(TERRAFORM_VERSION)/terraform_$(TERRAFORM_VERSION)_linux_amd64.zip
terraform/install: | $(BIN_DIR)
	@ echo "[$@]: Installing $(@D)..."
	@ echo "[$@]: TERRAFORM_URL=$(TERRAFORM_URL)"
	curl -sSL -o terraform.zip "$(TERRAFORM_URL)"
	unzip terraform.zip && rm -f terraform.zip && chmod +x terraform
	mv terraform "$(BIN_DIR)"
	terraform --version
	@ echo "[$@]: Completed successfully!"

terragrunt/install: TERRAGRUNT_URL ?= $(shell curl -sSL https://api.github.com/repos/gruntwork-io/terragrunt/releases/latest?access_token=4224d33b8569bec8473980bb1bdb982639426a92 | jq --raw-output  '.assets[] | select(.name=="terragrunt_linux_amd64") | .browser_download_url')
terragrunt/install: | $(BIN_DIR)
	@ echo "[$@]: Installing $(@D)..."
	@ echo "[$@]: TERRAGRUNT_URL=$(TERRAGRUNT_URL)"
	curl -sSL -o terragrunt "$(TERRAGRUNT_URL)"
	chmod +x terragrunt
	mv terragrunt "$(BIN_DIR)"
	terragrunt --version
	@ echo "[$@]: Completed successfully!"

node/install: NODE_SOURCE ?= https://deb.nodesource.com/setup_8.x
node/install:
	@ echo "[$@]: Installing $(@D)..."
	curl -sSL "$(NODE_SOURCE)" | bash -
	apt-get install nodejs -y
	npm --version
	@ echo "[$@]: Completed successfully!"

npm/install: node/install

semver/install: | guard/program/npm
	@ echo "[$@]: Installing $(@D)..."
	npm install -g semver
	@ echo "[$@]: Completed successfully!"

json/%: FIND_JSON := find . -not \( -name .terraform -prune \) -not \( -name .terragrunt-cache -prune \) -name '*.json' -type f
json/lint: | guard/program/jq
	@ echo "[$@]: Linting JSON files..."
	$(FIND_JSON) | $(XARGS_CMD) bash -c 'cmp {} <(jq --indent 4 -S . {}) || (echo "[{}]: Failed JSON Lint Test"; exit 1)'
	@ echo "[$@]: JSON files PASSED lint test!"

json/format:
	@ echo "[$@]: Formatting JSON files..."
	$(FIND_JSON) | $(XARGS_CMD) bash -c 'echo "$$(jq --indent 4 -S . "{}")" > "{}"'
	@ echo "[$@]: Successfully formatted JSON files!"

terraform/lint: | guard/program/terraform
	@ echo "[$@]: Linting Terraform files..."
	terraform fmt -check=true -diff=true
	@ echo "[$@]: Terraform files PASSED lint test!"

release/%: PRIOR_VERSION := $(shell git describe --abbrev=0 --tags 2> /dev/null)
release/%: RELEASE_VERSION := $(shell grep '^current_version' .bumpversion.cfg | sed 's/^.*= //')
release/%: | guard/program/semver

release/test: | guard/program/semver
	@ echo "[$@]: Checking version tag (prior) against version file (release)..."
	@ echo "[$@]: PRIOR_VERSION = $(PRIOR_VERSION)"
	@ echo "[$@]: RELEASE_VERSION = $(RELEASE_VERSION)"
	semver -r '> $(PRIOR_VERSION)' '$(RELEASE_VERSION)' > /dev/null

release/tag:
	@ if $(MAKE) release/test; then \
		echo "[$@]: Releasing version $(RELEASE_VERSION)"; \
		git tag $(RELEASE_VERSION); \
		git push --tags; \
	else \
		echo "[$@]: Version has not incremented, skipping tag release"; \
	fi

validate/%: | guard/program/zip %
	@ echo "[$@]: Validating '$*' configuration..."
	terragrunt validate --terragrunt-working-dir $* --terragrunt-source-update
	@ echo "[$@]: Completed successfully!"

plan/%: validate/% | %
	@ echo "[$@]: Creating plan for '$*' configuration..."
	terragrunt plan -out tfplan --terragrunt-working-dir $*
	@ echo "[$@]: Completed successfully!"

apply/%: plan/% | %
	@ echo "[$@]: Applying '$*' configuration..."
	terragrunt apply tfplan --terragrunt-working-dir $*
	@ echo "[$@]: Completed successfully!"

destroy/%: | %
	@ echo "[$@]: Destroying '$*' configuration..."
	terragrunt destroy --terragrunt-working-dir $* --terragrunt-source-update
	@ echo "[$@]: Completed successfully!"

event:
	@ if [ -n "$(IS_REVIEW)" ]; then \
		echo "[$@] REVIEW: Event is a pull request review, checking whether a release will be triggered..."; \
		if $(MAKE) release/test; then \
			echo "[$@]: Will release new version when merged."; \
		else \
			echo "[$@]: NOTE: An error message is expected when release/test displays the same versions."; \
			echo "[$@]: Version has not incremented; no release will be triggered when merged."; \
		fi; \
	elif [ -n "$(IS_BRANCH)" ]; then \
		echo "[$@] BRANCH: Handling deploy for event on branch '$(IS_BRANCH)'..."; \
		$(MAKE) release/tag; \
	elif [ -n "$(IS_TAG)" ]; then \
		echo "[$@] TAG: Handling deploy for event on tag '$(IS_TAG)'..."; \
	else \
		echo "[$@] ERROR: Unknown event type!"; \
		exit 1; \
	fi
.PHONY: event
