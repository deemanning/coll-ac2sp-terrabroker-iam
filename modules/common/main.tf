data "aws_partition" "current" {
  count = "${var.create_iam_roles ? 1 : 0}"
}

data "aws_region" "current" {
  count = "${var.create_iam_roles ? 1 : 0}"
}

data "aws_caller_identity" "current" {
  count = "${var.create_iam_roles ? 1 : 0}"
}

locals {
  overrides_map = {
    module_path    = "${path.module}"
    overrides_path = "../../partitions/${data.aws_partition.current.partition}/${data.aws_caller_identity.current.account_id}/overrides"
  }

  account_name = "${upper(replace(var.account_name, "-", "_"))}"

  instance_roles = [
    {
      name   = "INSTANCE_EXAMPLE"
      policy = "${var.stage}/policies/INSTANCE_EXAMPLE.template.json"
      trust  = "${var.stage}/trusts/EC2.template.json"
    },
    {
      name   = "INSTANCE"
      policy = "${var.stage}/policies/INSTANCE.template.json"
      trust  = "${var.stage}/trusts/EC2.template.json"
    },
    {
      name   = "INSTANCECODEDEPLOY"
      policy = "${var.stage}/policies/INSTANCECODEDEPLOY.template.json"
      trust  = "${var.stage}/trusts/EC2.template.json"
    },
    {
      name   = "INSTANCEEMR"
      policy = "${var.stage}/policies/INSTANCE.template.json"
      trust  = "${var.stage}/trusts/EC2.template.json"
    },
  ]

  parent_roles = [

  ]

  account_roles = [
    {
      name   = "SERVADMIN"
      policy = "${var.stage}/policies/SERVADMIN.template.json"
      trust  = "${var.stage}/trusts/CROSS_ACCOUNT.template.json"
    },
    {
      name   = "BUSINESS"
      policy = "${var.stage}/policies/BUSINESS.template.json"
      trust  = "${var.stage}/trusts/CROSS_ACCOUNT.template.json"
    },
    {
      name   = "CNDIS"
      policy = "${var.stage}/policies/CNDIS.template.json"
      trust  = "${var.stage}/trusts/CROSS_ACCOUNT.template.json"
    },
    {
      name   = "CNDISREADONLY"
      policy = "${var.stage}/policies/CNDISREADONLY.template.json"
      trust  = "${var.stage}/trusts/CROSS_ACCOUNT.template.json"
    },
    {
      name   = "CODEDEPLOYSERVICE"
      policy = "${var.stage}/policies/CODEDEPLOYSERVICE.template.json"
      trust  = "${var.stage}/trusts/CODEDEPLOY.template.json"
    },
    {
      name   = "CONFIG"
      policy = "${var.stage}/policies/CONFIG.template.json"
      trust  = "${var.stage}/trusts/CONFIG.template.json"
    },
    {
      name   = "CONFIGMANAGER"
      policy = "${var.stage}/policies/CONFIGMANAGER.template.json"
      trust  = "${var.stage}/trusts/CROSS_ACCOUNT.template.json"
    },
    {
      name   = "DATABASE"
      policy = "${var.stage}/policies/DATABASE.template.json"
      trust  = "${var.stage}/trusts/CROSS_ACCOUNT.template.json"
    },
    {
      name   = "EMRSERVICE"
      policy = "${var.stage}/policies/EMRSERVICE.template.json"
      trust  = "${var.stage}/trusts/elasticmapreduce.template.json"
    },
    {
      name   = "KEYMANAGER"
      policy = "${var.stage}/policies/KEYMANAGER.template.json"
      trust  = "${var.stage}/trusts/CROSS_ACCOUNT.template.json"
    },
    {
      name   = "MARKETPLACE"
      policy = "${var.stage}/policies/MARKETPLACE.template.json"
      trust  = "${var.stage}/trusts/CROSS_ACCOUNT.template.json"
    },
    {
      name   = "NETADMIN"
      policy = "${var.stage}/policies/NETADMIN.template.json"
      trust  = "${var.stage}/trusts/CROSS_ACCOUNT.template.json"
    },
    {
      name   = "PROJADMIN"
      policy = "${var.stage}/policies/PROJADMIN.template.json"
      trust  = "${var.stage}/trusts/CROSS_ACCOUNT.template.json"
    },
    {
      name   = "PROJADMINLIMITED"
      policy = "${var.stage}/policies/PROJADMINLIMITED.template.json"
      trust  = "${var.stage}/trusts/CROSS_ACCOUNT.template.json"
    },
    {
      name   = "S3ONLY"
      policy = "${var.stage}/policies/S3ONLY.template.json"
      trust  = "${var.stage}/trusts/CROSS_ACCOUNT.template.json"
    },
    {
      name   = "PROVADMIN"
      policy = "${var.stage}/policies/PROVADMIN.template.json"
      trust  = "${var.stage}/trusts/CROSS_ACCOUNT.template.json"
    },
    {
      name   = "TECHREADONLY"
      policy = "${var.stage}/policies/TECHREADONLY.template.json"
      trust  = "${var.stage}/trusts/CROSS_ACCOUNT.template.json"
    },
    {
      name   = "AC2SPMGT"
      policy = "${var.stage}/policies/AC2SPMGT.template.json"
      trust  = "${var.stage}/trusts/CROSS_ACCOUNT.template.json"
    },
  ]

  prereq_account_roles = [

  ]
}

################################
#
# INSTANCE role creation
#
################################

data "template_file" "instance_policies" {
  count = "${var.create_iam_roles ? length(local.instance_roles) : 0}"

  template = "${file(join("/", list(path.module, lookup(local.instance_roles[count.index], "policy"))))}"

  vars {
    account_name        = "${var.account_name}"
    account_id          = "${var.account_id}"
    partition           = "${data.aws_partition.current.partition}"
    region              = "${data.aws_region.current.name}"
    vpc                 = "${var.vpc_id}"
    igw                 = "${var.igw}"
    pcx                 = "${var.pcx}"
    bastion_sg          = "${var.bastion_sg}"
    url_suffix          = "${var.url_suffix}"
    cloudtrail_bucket   = "${var.cloudtrail_bucket}"
    provisioning_bucket = "${var.provisioning_bucket}"
  }
}

data "template_file" "instance_policy_trusts" {
  count = "${var.create_iam_roles ? length(local.instance_roles) : 0}"

  template = "${file(join("/", list(path.module, lookup(local.instance_roles[count.index], "trust"))))}"

  vars {
    partition          = "${data.aws_partition.current.partition}"
    account_id         = "${var.account_id}"
    trusted_account_id = "${var.parent_account_id}"
    url_suffix         = "${var.url_suffix}"
  }
}

data "external" "instance_overrides" {
  count = "${var.create_iam_roles ? length(local.instance_roles): 0}"

  program = ["python", "${path.module}/override_check.py", "-"]
  query   = "${merge(local.overrides_map, local.instance_roles[count.index])}"
}

data "template_file" "instance_policy_overrides" {
  count = "${var.create_iam_roles ? length(local.instance_roles) : 0}"

  template = "${file(lookup(data.external.instance_overrides.*.result[count.index], "policy_override"))}"

  vars {
    account_name        = "${var.account_name}"
    account_id          = "${var.account_id}"
    partition           = "${data.aws_partition.current.partition}"
    region              = "${data.aws_region.current.name}"
    vpc                 = "${var.vpc_id}"
    igw                 = "${var.igw}"
    pcx                 = "${var.pcx}"
    bastion_sg          = "${var.bastion_sg}"
    url_suffix          = "${var.url_suffix}"
    cloudtrail_bucket   = "${var.cloudtrail_bucket}"
    provisioning_bucket = "${var.provisioning_bucket}"
  }
}

data "template_file" "instance_policy_trust_overrides" {
  count = "${var.create_iam_roles ? length(local.instance_roles) : 0}"

  template = "${file(lookup(data.external.instance_overrides.*.result[count.index], "trust_override"))}"

  vars {
    partition          = "${data.aws_partition.current.partition}"
    account_id         = "${var.account_id}"
    trusted_account_id = "${var.parent_account_id}"
    url_suffix         = "${var.url_suffix}"
  }
}

data "aws_iam_policy_document" "instance_with_override_trusts" {
  count         = "${var.create_iam_roles ? length(local.instance_roles) : 0 }"
  source_json   = "${data.template_file.instance_policy_trusts.*.rendered[count.index]}"
  override_json = "${data.template_file.instance_policy_trust_overrides.*.rendered[count.index]}"
}

data "aws_iam_policy_document" "instance_with_override_policies" {
  count         = "${var.create_iam_roles ? length(local.instance_roles) : 0 }"
  source_json   = "${data.template_file.instance_policies.*.rendered[count.index]}"
  override_json = "${data.template_file.instance_policy_overrides.*.rendered[count.index]}"
}

resource "aws_iam_role" "instance" {
  count = "${var.create_iam_roles ? length(local.instance_roles) : 0}"

  name               = "${lookup(local.instance_roles[count.index], "name")}"
  assume_role_policy = "${element(data.aws_iam_policy_document.instance_with_override_trusts.*.json, count.index)}"
}

resource "aws_iam_policy" "instance" {
  count = "${var.create_iam_roles ? length(local.instance_roles) : 0}"

  name   = "${lookup(local.instance_roles[count.index], "name")}"
  policy = "${element(data.aws_iam_policy_document.instance_with_override_policies.*.json, count.index)}"
}

resource "aws_iam_role_policy_attachment" "instance" {
  count = "${var.create_iam_roles ? length(local.instance_roles) : 0}"

  role       = "${element(aws_iam_role.instance.*.id, count.index)}"
  policy_arn = "${element(aws_iam_policy.instance.*.arn, count.index)}"
}

resource "aws_iam_instance_profile" "instance" {
  count = "${var.create_iam_roles ? length(local.instance_roles) : 0}"

  name = "${lookup(local.instance_roles[count.index], "name")}"
  role = "${element(aws_iam_role.instance.*.id, count.index)}"
}

################################
#
# ACCOUNT role creation
#
################################

# variable assignment within base policies
data "template_file" "iam_account_policies" {
  count = "${var.create_iam_roles ? length(local.account_roles) : 0}"

  template = "${file(join("/", list(path.module, lookup(local.account_roles[count.index], "policy"))))}"

  vars {
    account_name        = "${var.account_name}"
    account_id          = "${var.account_id}"
    partition           = "${data.aws_partition.current.partition}"
    region              = "${data.aws_region.current.name}"
    vpc                 = "${var.vpc_id}"
    igw                 = "${var.igw}"
    pcx                 = "${var.pcx}"
    bastion_sg          = "${var.bastion_sg}"
    url_suffix          = "${var.url_suffix}"
    cloudtrail_bucket   = "${var.cloudtrail_bucket}"
    provisioning_bucket = "${var.provisioning_bucket}"
  }
}

# variable assignment within base trusts
data "template_file" "iam_account_policy_trusts" {
  count = "${var.create_iam_roles ? length(local.account_roles) : 0}"

  template = "${file(join("/", list(path.module, lookup(local.account_roles[count.index], "trust"))))}"

  vars {
    partition          = "${data.aws_partition.current.partition}"
    account_id         = "${var.account_id}"
    trusted_account_id = "${var.parent_account_id}"
    url_suffix         = "${var.url_suffix}"
  }
}

# find override policies and trusts
data "external" "iam_overrides" {
  count = "${var.create_iam_roles ? length(local.account_roles): 0}"

  program = ["python", "${path.module}/override_check.py", "-"]
  query   = "${merge(local.overrides_map, local.account_roles[count.index])}"
}

# format the policy overrides
data "template_file" "iam_account_policy_overrides" {
  count = "${var.create_iam_roles ? length(local.account_roles) : 0}"

  template = "${file(lookup(data.external.iam_overrides.*.result[count.index], "policy_override"))}"

  vars {
    account_name        = "${var.account_name}"
    account_id          = "${var.account_id}"
    partition           = "${data.aws_partition.current.partition}"
    region              = "${data.aws_region.current.name}"
    vpc                 = "${var.vpc_id}"
    igw                 = "${var.igw}"
    pcx                 = "${var.pcx}"
    bastion_sg          = "${var.bastion_sg}"
    url_suffix          = "${var.url_suffix}"
    cloudtrail_bucket   = "${var.cloudtrail_bucket}"
    provisioning_bucket = "${var.provisioning_bucket}"
  }
}

# format the trust overrides
data "template_file" "iam_account_policy_trust_overrides" {
  count = "${var.create_iam_roles ? length(local.account_roles) : 0}"

  template = "${file(lookup(data.external.iam_overrides.*.result[count.index], "trust_override"))}"

  vars {
    partition          = "${data.aws_partition.current.partition}"
    account_id         = "${var.account_id}"
    trusted_account_id = "${var.parent_account_id}"
    url_suffix         = "${var.url_suffix}"
  }
}

# define the IAM role trust policy document
data "aws_iam_policy_document" "account_with_override_trusts" {
  count         = "${var.create_iam_roles ? length(local.account_roles) : 0 }"
  source_json   = "${data.template_file.iam_account_policy_trusts.*.rendered[count.index]}"
  override_json = "${data.template_file.iam_account_policy_trust_overrides.*.rendered[count.index]}"
}

# define the IAM policy document
data "aws_iam_policy_document" "account_with_override_policies" {
  count         = "${var.create_iam_roles ? length(local.account_roles) : 0 }"
  source_json   = "${data.template_file.iam_account_policies.*.rendered[count.index]}"
  override_json = "${data.template_file.iam_account_policy_overrides.*.rendered[count.index]}"
}

# create the IAM role
resource "aws_iam_role" "account" {
  count = "${var.create_iam_roles ? length(local.account_roles) : 0}"

  name               = "${lookup(local.account_roles[count.index], "name")}"
  assume_role_policy = "${element(data.aws_iam_policy_document.account_with_override_trusts.*.json, count.index)}"
}

# create the IAM role policy
resource "aws_iam_policy" "account" {
  count = "${var.create_iam_roles ? length(local.account_roles) : 0}"

  name   = "${lookup(local.account_roles[count.index], "name")}"
  policy = "${element(data.aws_iam_policy_document.account_with_override_policies.*.json, count.index)}"
}

# attach the IAM policy to the IAM role
resource "aws_iam_role_policy_attachment" "account" {
  count = "${var.create_iam_roles ? length(local.account_roles) : 0}"

  role       = "${element(aws_iam_role.account.*.id, count.index)}"
  policy_arn = "${element(aws_iam_policy.account.*.arn, count.index)}"
}

################################
#
# PREREQUISITE ACCOUNT roles
#
################################

data "template_file" "iam_prereq_policy_trusts" {
  count = "${var.create_iam_roles ? length(local.prereq_account_roles) : 0}"

  template = "${file(join("/", list(path.module, lookup(local.prereq_account_roles[count.index], "trust"))))}"

  vars {
    partition          = "${data.aws_partition.current.partition}"
    account_id         = "${var.account_id}"
    trusted_account_id = "${var.parent_account_id}"
    url_suffix         = "${var.url_suffix}"
  }
}

resource "aws_iam_role" "prereq" {
  count = "${var.create_iam_roles ? length(local.prereq_account_roles) : 0}"

  name               = "${lookup(local.prereq_account_roles[count.index], "name")}"
  assume_role_policy = "${element(data.template_file.iam_prereq_policy_trusts.*.rendered, count.index)}"
}

resource "aws_iam_role_policy_attachment" "prereq" {
  count = "${var.create_iam_roles ? length(local.prereq_account_roles) : 0}"

  role       = "${aws_iam_role.prereq.*.name[count.index]}"
  policy_arn = "${lookup(local.prereq_account_roles[count.index], "policy")}"
}

################################
#
# PARENT role creation
#
################################

# variable assignment within base policies
data "template_file" "iam_parent_policies" {
  count = "${var.create_iam_roles ? length(local.parent_roles) : 0}"

  template = "${file(join("/", list(path.module, lookup(local.parent_roles[count.index], "policy"))))}"

  vars {
    account_id        = "${var.account_id}"
    partition         = "${data.aws_partition.current.partition}"
    url_suffix        = "${var.url_suffix}"
    cloudtrail_bucket = "${var.cloudtrail_bucket}"
  }
}

# variable assignment within base trusts
data "template_file" "iam_parent_policy_trusts" {
  count = "${var.create_iam_roles ? length(local.parent_roles) : 0}"

  template = "${file(join("/", list(path.module, lookup(local.parent_roles[count.index], "trust"))))}"

  vars {
    partition          = "${data.aws_partition.current.partition}"
    account_id         = "${var.account_id}"
    trusted_account_id = "${var.parent_account_id}"
    url_suffix         = "${var.url_suffix}"
  }
}

resource "aws_iam_role" "parent" {
  count = "${var.create_iam_roles ? length(local.parent_roles) : 0}"

  provider = "aws.parent"

  name               = "${lookup(local.parent_roles[count.index], "name")}"
  assume_role_policy = "${element(data.template_file.iam_parent_policy_trusts.*.rendered, count.index)}"
}

resource "aws_iam_role_policy" "parent" {
  count = "${var.create_iam_roles ? length(local.parent_roles) : 0}"

  provider = "aws.parent"

  name   = "${lookup(local.parent_roles[count.index], "name")}"
  role   = "${element(aws_iam_role.parent.*.id, count.index)}"
  policy = "${element(data.template_file.iam_parent_policies.*.rendered, count.index)}"
}
