"""IAM policy override check."""
from __future__ import print_function

import argparse
import json
import os
import sys


def override_check(module_path, overrides_path, template_path):
    """Find overrides. but better."""
    full_template_path = "/".join([
        module_path,
        template_path
    ]).replace("//", "/")

    override_file = "/".join([
        module_path,
        overrides_path,
        template_path
    ]).replace("//", "/")

    return (
        override_file if os.path.isfile(override_file) else full_template_path
    )


def main(**json_args):
    """Return json args."""
    json_args['policy_override'] = override_check(
        module_path=json_args['module_path'],
        overrides_path=json_args['overrides_path'],
        template_path=json_args['policy']
    )
    json_args['trust_override'] = override_check(
        module_path=json_args['module_path'],
        overrides_path=json_args['overrides_path'],
        template_path=json_args['trust']
    )
    return json_args


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "json", type=argparse.FileType("r"), default=sys.stdin,
        help="Parses input from a json file or stdin"
    )

    args = parser.parse_args()

    json_args = {}
    with args.json as fp_:
        json_args = json.load(fp_)
    sys.exit(print(json.dumps(main(**json_args))))
