variable "create_iam_roles" {
  description = "Controls whether to create the common IAM roles"
  default     = true
}

variable "url_suffix" {
  description = "URL Suffix associated with the AWS partition"
  type        = "string"
  default     = "amazonaws.com"
}

variable "name" {
  description = "Name of the account"
  type        = "string"
  default     = ""
}

variable "account_id" {
  description = "AWS account ID of the target account"
  type        = "string"
  default     = ""
}

variable "parent_account_id" {
  description = "AWS account ID of the parent account"
  type        = "string"
  default     = ""
}

variable "vpc_id" {
  description = "ID of the VPC"
  type        = "string"
  default     = ""
}

variable "igw" {
  description = "ID of the Internet Gateway"
  type        = "string"
  default     = ""
}

variable "pcx" {
  description = "ID of the VPC Peering Connection"
  type        = "string"
  default     = ""
}

variable "bastion_sg" {
  description = "ID of the Bastion Security Group"
  type        = "string"
  default     = ""
}

variable "cloudtrail_bucket" {
  description = "Name of the S3 bucket storing CloudTrail logs"
  type        = "string"
  default     = ""
}

variable "provisioning_bucket" {
  description = "Name of S3 bucket hosting provisioning config data"
  type        = "string"
  default     = ""
}

variable "stage" {
  description = "Stage for the account"
  type        = "string"
}
