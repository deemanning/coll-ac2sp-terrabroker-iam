# Terrabroker-IAM module

This module is designed to apply the CBT baseline IAM roles, policies, and
associated trusts.

Additionally, it will seamlessly apply overrides to account policy and trust
templates that are contained within the corresponding account's `overrides/policies`
and `overrides/trusts` directories within this repo.

## Project Structure

Account baseline policies and trusts are stored in the following project
directories:

- Policies: `modules/common/${stage}/policies/`
- Trusts: `modules/common/${stage}/trusts/`

Account overrides are applied on a per-account basis and are stored in the
following directories:

- Policies: `partitions/${partition}/${account_id}/overrides/policies`
- Trusts: `partitions/${partition}/${account_id}/overrides/trusts`

## Override Guidance

- Overrides work by matching filenames and SIDs.
- Baseline policies will always be applied _unless_ there is a corresponding
  override policy in the account's overrides location, see [Project Structure](project-structure).
- The baseline filename and the override filename must be the same for the
  override to be effective.
- A SID in an override policy will match and override a statement with the same
  SID in the corresponding baseline policy.
- A SID that does not match any statement in the baseline policy will cause the
  statement to be appended to the resultant policy.
- To create a baseline policy statement that cannot be overridden, use a blank
  SID.

## How To Create an IAM Override Configuration

1. Ensure you have followed the all the steps outlined in the [Terrabroker README][terrabroker-readme]
   up to the `Apply an Account Configuration` section.

2. Clone this project from CodeCommit:

    ```bash
    git clone https://git-codecommit.us-east-1.amazonaws.com/v1/repos/terrabroker-iam
    ```

3. Update your clone:

    ```bash
    cd terrabroker-iam
    git checkout master
    git pull
    ```

4. Create a new branch:

    ```bash
    git checkout -b <branch_name>
    ```

5. Copy your desired override policies or trusts from the baseline location:

    - `modules/common/${stage}/policies/`
    - `modules/common/${stage}/trusts/`

    to the following folder(s):

    - `partitions/${partition}/${account_id}/overrides/policies`
    - `partitions/${partition}/${account_id}/overrides/trusts`

    > **Warning: Any override policy must have the same filename as the policy
    > it overrides.**
    >
    > Example:
    >
    > Target policy to override:
    > `modules/common/dev/policies/INSTANCE_EXAMPLE.template.json`
    >
    > Expected policy name:
    > `partitions/aws/${account_id}/overrides/policies/INSTANCE_EXAMPLE.template.json`

6. Edit the policy to meet your override requirements. Be aware that overrides
   work on a SID-by-SID basis, not the whole file. See the section [Override Guidance][override-guidance].

7. Ensure that your policies are written in valid json. From the root of the
   `terrabroker-iam` directory run:

    ```bash
    make json/format
    ```

    > **Note: It may be useful at this step to validate your policy using the
    > AWS IAM policy builder as well**

8. Commit the change:

    ```bash
    git add .
    git commit -m 'Adds overrides for <account_id>'
    ```

9. Push the new branch:

    ```bash
    git push -u origin <branch_name>
    ```

10. Open a [Pull Request](https://console.aws.amazon.com/codecommit/home?region=us-east-1#/repository/terrabroker-iam/pull-requests/new)
    and request a review.

## Review the IAM Override Configuration

1. Open the [Pull Request in CodeCommit](https://console.aws.amazon.com/codecommit/home?region=us-east-1#/repository/terrabroker-iam/pull-requests).
2. Spot check the new account configuration...
3. Request changes if necessary.
4. Merge the Pull Request when everything looks good.

## Apply IAM Overrides

IAM policies are _applied_ through the Terrabroker project. To apply the IAM
policies, follow all the steps outlined in the section [Apply the terrabroker configuration][terrabroker-readme]
in the Terrabroker README.

[terrabroker-readme]: https://console.aws.amazon.com/codecommit/home?region=us-east-1#/repository/terrabroker/browse/HEAD/--/README.md
